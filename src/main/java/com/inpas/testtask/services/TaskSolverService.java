package com.inpas.testtask.services;

/**
 * Created by camel on 18.03.17.
 */
public class TaskSolverService {

    public static long solve(int[] map) {
        int maxHeightIndex = 0;

        // finding index of max height on map
        for (int i = 1; i < map.length; ++i) {
            if (map[i] > map[maxHeightIndex]) {
                maxHeightIndex = i;
            }
        }

        /* Going level by level from max height level. On each level accumulating number of hole-blocks going
         * between hill blocks. Such level-local holes then accumulated in map-global volume of holes.
         */
        long volume = 0;
        for (int level = map[maxHeightIndex]; level > 0; --level) {
            int localHolesVolume = 0;
            boolean isAfterHill = false; // initially there are no viewed blocks
            for (int j = 0; j < map.length; ++j) {
                if ((map[j] < level) && isAfterHill) { // if there is a hole-block going after hill-block
                    ++localHolesVolume; // accumulating volume of level-local hole
                } else if (map[j] >= level) { // current block is hill block
                    if (isAfterHill && (map[j - 1] < level)) { // if there is an end of hole going from hill
                        volume += localHolesVolume; // accumulating volume of all holes on map
                        localHolesVolume = 0;
                    } else if (j != map.length - 1) { // if next block isn't last block
                        if (map[j + 1] < level) { // if next block is hole-block
                            isAfterHill = true; // next block is hole-block after hill-block
                        } else {
                            isAfterHill = false; // next block is hill-block too
                        }
                    }
                }
            }
        }

        return volume;
    }

}
