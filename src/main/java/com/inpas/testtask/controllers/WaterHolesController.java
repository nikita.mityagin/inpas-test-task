package com.inpas.testtask.controllers;

/**
 * Created by camel on 17.03.17.
 */

import com.inpas.testtask.controllers.models.InputArray;
import com.inpas.testtask.services.TaskSolverService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@SuppressWarnings("unused")
public class WaterHolesController {

    @GetMapping("/input")
    public String getInputPage(Model model) {
        model.addAttribute("inputArray", new InputArray());

        return "input";
    }

    @PostMapping("/solve")
    public String solve(@ModelAttribute("inputArray") InputArray array, Model model) {
        String[] numbersAsStrings = array.getArray().split(",");
        int[] numbers = new int[numbersAsStrings.length];

        for (int i = 0; i < numbersAsStrings.length; ++i) {
            numbers[i] = Integer.valueOf(numbersAsStrings[i]);
        }

        model.addAttribute("solution", TaskSolverService.solve(numbers));

        return "solution";
    }
}
