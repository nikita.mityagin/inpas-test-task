package com.inpas.testtask.controllers.models;

import lombok.Data;

/**
 * Created by camel on 18.03.17.
 */
@Data
public class InputArray {
    private String array;
}
