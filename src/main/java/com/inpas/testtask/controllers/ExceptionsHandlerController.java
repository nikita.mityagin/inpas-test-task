package com.inpas.testtask.controllers;

/**
 * Created by camel on 18.03.17.
 */

import lombok.extern.log4j.Log4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Log4j
@ControllerAdvice
public class ExceptionsHandlerController {

    @ExceptionHandler(Exception.class)
    @ResponseBody public String handleError(HttpServletRequest req, Exception ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);

        return "error";
    }

}
