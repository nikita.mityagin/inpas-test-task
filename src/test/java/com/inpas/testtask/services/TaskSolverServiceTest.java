package com.inpas.testtask.services;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by camel on 18.03.17.
 */
public class TaskSolverServiceTest {

    @Test(expected = NullPointerException.class)
    public void testNullInput() {
        int[] map = null;

        assertEquals(0, TaskSolverService.solve(map));
    }

    @Test
    public void testArrayWithOneHeight() {
        int[] map = {1};

        assertEquals(0, TaskSolverService.solve(map));
    }

    @Test
    public void testFlatMap() {
        int[] map = {1, 1, 1, 1, 1, 1, 1, 1};

        assertEquals(0, TaskSolverService.solve(map));
    }

    @Test
    public void testMapWithHolesAndHighLonelyHill() {
        int[] map = {1, 2, 1, 0, 1, 0, 2, 0, 0, 1, 2, 3, 2, 1}; // high lonely hill is 3

        assertEquals(11, TaskSolverService.solve(map));
    }

    @Test
    public void testMapWithSeveralHighHillsOnTheSameLevel() {
        int[] map = {4, 1, 0, 2, 1, 2, 3, 4, 3, 1, 2, 4, 2, 3}; // same level of high hills is 4

        assertEquals(22, TaskSolverService.solve(map));
    }


}